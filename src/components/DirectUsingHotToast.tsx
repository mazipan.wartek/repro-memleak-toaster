import { Button } from "@wartek-id/button"
import toast, { Toaster } from "react-hot-toast";

export const DirectUsingHotToast = () => {

  return (
    <div className="p-8">
      <Button onClick={() => toast('test')}>Direct use react-hot-toast</Button>
      <Toaster />
    </div>
  )
}