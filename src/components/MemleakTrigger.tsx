import { Button } from "@wartek-id/button"
import { useToast } from "@wartek-id/toast";

export const MemleakTrigger = ({ title = '' }) => {
  const toast = useToast();

  return (
    <div className="p-8">
        <p className="mb-2">{title}</p>
        <Button onClick={() => {
          console.log('before click')
          toast({
            message: "Hello World",
            type: "success",
            duration: 5000,
            actionLabel: 'Close',
            placement: 'top-center',
            ellipsis: true
          })
          console.log('finish click')
        }}>Click Me To Trigger Memleak</Button>
      </div>
  )
}