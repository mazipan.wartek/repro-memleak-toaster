'use client'

import { DirectUsingHotToast } from "@/components/DirectUsingHotToast";
import Link from "next/link";

export default function Home() {
  return (
    <>
      <div className="ml-8 my-4 flex gap-4">
        <Link href="/non-memleak" className="underline text-blue-50">To Non-Memleak Page</Link>
        <Link href="/memleak" className="underline text-blue-50">To Memleak Page</Link>
      </div>

      <DirectUsingHotToast />
    </>
  );
}
