'use client'

import { DirectUsingHotToast } from "@/components/DirectUsingHotToast";
import { MemleakTrigger } from "@/components/MemleakTrigger";
import { ToastProvider } from "@wartek-id/toast";
import Link from "next/link";

export default function Home() {
  return (
    <>
      <div className="ml-8 my-4 flex gap-4">
        <Link href="/non-memleak" className="underline text-blue-50">To Non-Memleak Page</Link>
        <Link href="/memleak" className="underline text-blue-50">To Memleak Page</Link>
      </div>

      <ToastProvider
        autoDismiss={true}
        autoDismissTimeout={5000}
        placement="top-center">
        <MemleakTrigger title="This action will freeze your page" />
      </ToastProvider>
    </>
  );
}
