const defaultTheme = require('tailwindcss/defaultTheme')
const configs = require('@wartek-id/tailwind-config/src/guru.desktop')

module.exports = {
  mode: 'jit',
  content: [
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      colors: {
        ...configs.colors,
      },
      textColors: { ...configs.colors, ...configs.textColor },
      backgroundColor: {
        ...configs.colors,
        ...configs.backgroundColor,
      },
      borderColor: {
        ...configs.colors,
        ...configs.borderColor,
      },
      fontFamily: {
        sans: ['Inter', ...defaultTheme.fontFamily.sans],
        body: ['Inter', ...defaultTheme.fontFamily.sans],
      },
    },
  },
  safelist: [],
  plugins: [],
}
